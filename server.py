#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import sys
import socketserver
from datetime import datetime, date, time, timedelta

if len(sys.argv) == 2:
    Puerto = int(sys.argv[1])
    FORMAT = "%H:%M:%S %d-%m-%Y"
else:
    sys.exit ("Needed: python3 server.py <port>")

class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    SIP Register Handler
    """
    dicc = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        user_dicc = {"addres": "", "expires": ""}
        Ip = self.client_address[0]
        Puerto_cliente = self.client_address[1]
        lista = []
        for line in self.rfile:
            lista.append(line.decode("utf-8"))
        texto = lista[0].split(" ")

        if texto[0] == "REGISTER":
            usuario = texto[1].split(":")[1]
            texto = lista[1].split()
            expires = texto[1].split("\r\n")[0]
            expires_date = datetime.now() + timedelta(seconds = int(expires))
            user_dicc["addres"] = Ip + ":" + str(Puerto_cliente)
            self.wfile.write(b"SIP/2.0 200 OK/r/n/r/n")
            user_dicc["expires"] = expires_date.strftime(FORMAT)
        for line in self.rfile:
            print("El cliente nos manda ", line.decode('utf-8'))
            if int(expires) == 0:
                try:
                    del self.dicc[usuario]
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                except KeyError:
                    pass
            else:
                self.dicc[usuario] = user_dicc
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', Puerto), SIPRegisterHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
