#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

SERVER = sys.argv[1]
PORT = int(sys.argv[2])
ACTION1 = str.upper(sys.argv[3])
USUARIO = sys.argv[4]
ACTION2 = int(sys.argv[5])
ACTION1_SIP = ACTION1 + " sip:" + USUARIO + " SIP/2.0\r\n"
ACTION2_SIP = "Expires: " + str(ACTION2) + "\r\n\r\n"

if ACTION1 == "REGISTER":
    LINE = ACTION1_SIP + ACTION2_SIP
else:
    sys.exit ("Needed: REGISTER")
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print(data.decode('utf-8'))

print("Socket terminado.")
